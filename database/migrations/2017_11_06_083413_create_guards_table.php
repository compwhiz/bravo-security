<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGuardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('guards', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('phonenumber')->nullable();
            $table->string('idnumber')->nullable();
            $table->string('email')->nullable();
            $table->unsignedInteger('client_id')->nullable();
            $table->date('employedDate');
            $table->date('terminationDate')->nullable();
            $table->string('bank_account')->nullable();
            $table->string('employmentID')->nullable();
            $table->foreign('client_id')->references('id')->on('clients');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('guards');
    }
}
