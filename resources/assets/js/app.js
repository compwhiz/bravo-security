
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('attendance', require('./components/attendance/attendance'));
Vue.component('update-payments', require('./components/payments/index'));
Vue.component('update-invoice', require('./components/payments/updateinvoice'));
Vue.component('transfer', require('./components/transfers/transfer'));
Vue.component('clients', require('./components/client/index'));

const app = new Vue({
    el: '#app'
});
