<html>
<head>
    <title>Bravo Security</title>
    <script language="JavaScript" type="text/javascript">
        /*setTimeout("window.print();", 10000);*/
    </script>
    <style>
        body {
            padding: 0px;
            margin: 0px;
            font-size: 12px;
        }

        table.data {
            font-family: Verdana;
            font-size: 9px;
            empty-cells: show;
            border: 1px solid #000;
            border-collapse: collapse;
            border-spacing: 0.5rem;
            empty-cells: show;
        }

        table.data td {
            border: 1px solid black;
        }

        table.data td.header {
            background-color: #EDECEB;
            font-size: 13px !important;
            font-weight: bold;
        }

        table.data td.abottom {
            vertical-align: bottom;
            /*font-size: 10px;*/
        }

        span.title {
            font-size: 14px;
            font-weight: bold;
        }

        footer {
            position: fixed;
            bottom: 60px;
            left: 0px;
            right: 0px;
            height: 50px;
        }

        @media all {
            .page-break {
                display: none;
            }
        }

        @media print {
            .page-break {
                display: block;
                page-break-before: always;
                margin: 0px;
                padding: 0px;
            }
        }

        @media screen {
            .page-break {
                display: block;
                page-break-before: always;
                margin: 5px;
                padding: 5px;
            }
        }


    </style>

</head>
<body>
<table cellpadding="2" cellspacing="0" width="100%" class="data">
    <tr>
        <td colspan="9">

            <table width="100%" border=0 cellspacing="0" cellpadding="1" class="data">

                <tr>
                    <td rowspan="6" align="center">
                        <img src="{{ asset('BRAVOLOGO.png') }}" height="100px">
                    </td>
                </tr>
                <tr>
                    <td valign="top" colspan="2"><span class="title">Bravo Two Zero Security Service</span></td>
                </tr>

                <tr>
                    <td colspan="2"><b>Address : P.O BOX 132 &mdash; 90138, Makindu</b></td>
                </tr>

                <tr>
                    <td colspan="2"><b>Tel : 0722 393 529 | 0713 642 175</b></td>
                </tr>

                <tr>
                    <td colspan="2"><b>Your security is our commitment! </b></td>
                </tr>

                <tr>
                    <td colspan="4"><b>https://bravosecurity.co.ke</b></td>
                </tr>

                <tr>
                    <td><b>CLIENT REMITANCE</b></td>
                    <td colspan="4"><b>FOR AUGUST 2018</b></td>
                </tr>

            </table>

        </td>
    </tr>

</table>
<table cellpadding="2" cellspacing="0" width="100%" class="data">
    <tr>
        <td class="header" colspan="6"></td>
        <td class="header" colspan="2" align="center">COLLECTIONS</td>
        <td class="header"></td>
    </tr>
    <tr>
        <td class="header">Client</td>
        <td class="header">Period</td>
        <td class="header">Amount Owed</td>
        <td class="header">Invoice Number</td>
        <td class="header">Date Paid</td>
        <td class="header">Amount Paid</td>
        <td class="header">Cash</td>
        <td class="header">Cheque</td>
        <td class="header">Collecting Officer</td>
    </tr>
    <tbody>

    @foreach($clientE as $clients)
        <tr>
            <td>
                <strong>{{ strtoupper($clients->name) }}</strong>
                <br>
                <span style="float: right !important;font-size: small;color: grey !important;">
                    <strong>
                        {{ strtoupper($clients->guards) }}
                        {{ \Illuminate\Support\Str::plural('Guard',$clients->guards) }}</strong>
                </span>
            </td>
            <td valign="top">
                {{ \Carbon\Carbon::today()->subMonth(1)->startOfMonth()->toFormattedDateString() }}

                &mdash;

                {{ \Carbon\Carbon::today()->subMonth(1)->endOfMonth()->toFormattedDateString() }}
            </td>
            <td valign="top">
                <strong>KES {{ number_format($clients->dailyrate * $clients->guards,2)  }}</strong>
            </td>
            <td valign="top"><strong>{{ $clients->payments->first()['invoice'] }}</strong></td>
            <td valign="top" colspan="5"></td>

        </tr>
        @foreach($clients->payments as $k)
            @foreach($k->payment as $l)
                <tr>
                    <td colspan="4" style="background-color: black;"></td>
                    <td><strong>{{ \Carbon\Carbon::parse($l->date)->toFormattedDateString() }}</strong></td>
                    <td><strong>KES {{ number_format($l->amount,2) }}</strong></td>
                    <td>
                        <strong>
                            @if($l->chequeno)
                            @else
                                KES {{ number_format($l->amount,2) }}
                            @endif
                        </strong>
                    </td>
                    <td><strong>{{ $l->chequeno }}</strong></td>
                    <td>DENNIS MWANZA MUTUA</td>
                </tr>
            @endforeach
        @endforeach
    @endforeach
    <tr>
        <td colspan="9"></td>
    </tr>
    <tr>
        <td colspan="4" style="border-bottom: transparent !important;"></td>
        <td><strong>PAYROLL</strong>:</td>
        <td><strong>157,345.00</strong></td>
    </tr>
    <tr>
        <td colspan="4"></td>

        <td><strong>DEDUCTIONS</strong>: <br>
            <small>UNIFORM</small>
        </td>
        <td valign="top"><strong>16,500.00</strong></td>
    </tr>
    <tr>
        <td colspan="4"></td>
        <td><strong>TOTALS</strong>:</td>
        <td style="border-bottom: double"><strong>140,845.00</strong></td>
    </tr>
    <tr>

        <td colspan="9">
            <p style="font-size: 15px;">ADDITIONAL NOTES: </p></td>
    </tr>
    <tr>

        <td colspan="9">
            <p style="font-size: 14px;"> DENNIS MWANZA BANKED <strong>KES 28,100 </strong> to BRAVO ACC. <strong>1161299874</strong>
                ref <strong>TT182509CV82</strong>
                on <strong> 07/09/2018</strong> Against a collected <strong>KES 37,000</strong></p>
        </td>

    </tr>
    <tr align="left">
        <td colspan="4" align="left"></td>
        <td colspan="6" align="left">DENNIS M. MWANZA ACCOUNTING FOR DEFICIT OF KES. 8,900.00</td>
    </tr>
    <tr>
        <td colspan="4"></td>
        <td>PAID SAMMY NGATI</td>
        <td><strong>6,000.00</strong></td>
    </tr>
    <tr>
        <td colspan="4"></td>
        <td>MUTULU MBONDO - Bought food. <strong>TO BE RECOVERED.</strong></td>
        <td><strong>1,000.00</strong></td>
    </tr>
    <tr>
        <td colspan="4"></td>
        <td>FUEL TO KAMBU</td>
        <td><strong>400.00</strong></td>
    </tr>
    <tr>
        <td colspan="4"></td>
        <td>MOTORCYCLE REPAIR DURING KAMBU TRIP</td>
        <td><strong>550.00</strong></td>
    </tr>
    <tr>
        <td colspan="4"></td>
        <td>FUEL AND CREDIT</td>
        <td><strong>300.00</strong></td>
    </tr>
    <tr>
        <td colspan="4"></td>
        <td>MPESA CHARGES</td>
        <td><strong>200.00</strong></td>
    </tr>
    <tr>
        <td colspan="4"></td>
        <td>CASH</td>
        <td><strong>450.00</strong></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td colspan="4"></td>
        <td><strong>TOTAL.</strong></td>
        <td><strong>8,450.00</strong></td>
    </tr>
    </tbody>


</table>
</body>
</html>
