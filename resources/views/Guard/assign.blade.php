@extends('layouts.bravo')
@section('content')
    <div class="container">
        <div class="row">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="col-md-12">
                        <form action="" method="post">
                            {{-- Label:Guard , Attributes:guard  --}}
                            <div class="col-md-6 form-group{{ $errors->has('guard') ? ' has-error' : '' }}">
                                {{--<label for="guard" class="control-label">Guard</label>--}}
                                <div>
                                    <div class="form-group">
                                        <label for="guard">Guard</label>
                                        <select class="form-control" name="guard" id="guard">
                                            @foreach (\App\Guards::all() as $guard)
                                                <option value="{{ $guard->id }}">{{ $guard->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    @if ($errors->has('guard'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('guard') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            {{-- Label:Client , Attributes:client  --}}
                            <div class="col-md-6 form-group{{ $errors->has('client') ? ' has-error' : '' }}">
                                {{--<label for="client" class="control-label">Client</label>--}}
                                <div>
                                    <div class="form-group">
                                        <label for="client">Client</label>
                                        <select class="form-control" name="client" id="client">
                                            @foreach (\App\Client::all() as $client)
                                                <option value="{{ $client->id }}">{{ $client->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>                                    @if ($errors->has('client'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('client') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-12">
                                <button class="btn btn-default">Assign Guard</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection