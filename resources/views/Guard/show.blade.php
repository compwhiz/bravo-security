@extends('layouts.bravo')
@section('title')
    Guard: ({{ $guard->name }} | {{ $guard->employmentID }})
@stop
<?php /** @var \App\Guards $guard */ ?>
@section('content')
    <div class="row">
        <div class="col-lg-6 col-sm-6 col-xs-12">
            <div class="white-box">
                <h3 class="box-title">{{ $guard->name }}</h3>
                <p class="text-muted">Phone: <code>{{ $guard->phonenumber }}</code></p>
                <p class="text-muted">Email: <code>{{ $guard->email }}</code></p>
                <p class="text-muted">ID: <code>{{ $guard->idnumber }}</code></p>
                <p class="text-muted">Employment Date: <code>{{ $guard->employedDate }}</code></p>
                <p class="text-muted">Bank Account: <code>{{ $guard->bank_account }}</code></p>

                <div class="table-responsive">
                    <table class="table table-condensed table-hover">
                        <thead>
                        <tr>
                            <th>Client Name</th>
                            <th>Start Date</th>
                            <th>End Date</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($guard->assignments as $value)
                            <tr>
                                <td>{{ $value->client->name}}</td>
                                <td>{{ $value->startDate->toFormattedDateString() }}</td>
                                <td>{{ $value->endDate }}</td>
                                <td>
                                    <form action="" method="post">
                                        <a href="{{ route('disengageGuard',$guard->id) }}"
                                           class="btn btn-danger waves-effect waves-light">
                                            <span>Disengange</span> <i
                                                    class="fa fa-forward m-l-5"></i></a>
                                    </form>

                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>
    <div class="row">
        <div class="col-lg-12 col-sm-12 col-xs-12">
            <div class="white-box">
                <h3 class="box-title">Payments</h3>
                <p class="text-muted m-b-30">Assign a client to the guard.</p>


            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 col-sm-12 col-xs-12">
            <div class="white-box">
                <h3 class="box-title">Guard History</h3>
                <p class="text-muted m-b-30">Assign a client to the guard.</p>
                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Start Date</th>
                        <th>End Date</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($history->assignments as $assignment)
                        <tr>
                            <td>{{ $assignment->trashedClient->name }}</td>
                            <td>{{ $assignment->startDate}}</td>
                            <td>{{ $assignment->endDate}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>


            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 col-sm-12 col-xs-12">
            <div class="white-box">
                <h3 class="box-title">Payroll</h3>
                <p class="text-muted m-b-30">Assign a client to the guard.</p>

                <div class="row">
                    <div class="col-md-12">
                        <form action="{{ route('guard.update',$guard->id) }}" method="post">
                            {{ csrf_field() }}
                            {{ method_field('PATCH') }}
                            <input type="hidden" name="id" value="{{ $guard->id }}">
                            <div class="col-md-6 form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                <label for="name" class="control-label">Name</label>
                                <div>
                                    <input id="name" type="text" class="form-control" name="name"
                                           value="{{ $guard->name }}">
                                    @if ($errors->has('name'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            {{-- Label:Phone Number , Attributes:phonenumber  --}}
                            <div class="col-md-6 form-group{{ $errors->has('phonenumber') ? ' has-error' : '' }}">
                                <label for="phonenumber" class="control-label">Phone Number</label>
                                <div>
                                    <input id="phonenumber" type="text" class="form-control" name="phonenumber"
                                           value="{{ $guard->phonenumber }}">
                                    @if ($errors->has('phonenumber'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('phonenumber') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            {{-- Label:ID number , Attributes:idnumber  --}}
                            <div class="col-md-6 form-group{{ $errors->has('idnumber') ? ' has-error' : '' }}">
                                <label for="idnumber" class="control-label">ID number</label>
                                <div>
                                    <input id="idnumber" type="text" class="form-control" name="idnumber"
                                           value="{{ $guard->idnumber }}">
                                    @if ($errors->has('idnumber'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('idnumber') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            {{-- Label:Employment ID , Attributes:employmentID  --}}
                            <div class="col-md-6 form-group{{ $errors->has('employmentID') ? ' has-error' : '' }}">
                                <label for="employmentID" class="control-label">Employment ID</label>
                                <div>
                                    <input id="employmentID" type="text" class="form-control" name="employmentID"
                                           value="{{ $guard->employmentID }}">
                                    @if ($errors->has('employmentID'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('employmentID') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            {{-- Label:Bank Account , Attributes:bank_account  --}}
                            <div class="col-md-6 form-group{{ $errors->has('bank_account') ? ' has-error' : '' }}">
                                <label for="bank_account" class="control-label">Bank Account</label>
                                <div>
                                    <input id="bank_account" type="text" class="form-control" name="bank_account"
                                           value="{{ $guard->bank_account }}">
                                    @if ($errors->has('bank_account'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('bank_account') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            {{-- Label:Employed Date , Attributes:employeddate  --}}
                            <div class="col-md-6 form-group{{ $errors->has('employeddate') ? ' has-error' : '' }}">
                                <label for="employeddate" class="control-label">Employed Date</label>
                                <div>
                                    <input id="employeddate" type="date" class="form-control" name="employeddate"
                                           value="{{ $guard->employedDate }}">
                                    @if ($errors->has('employeddate'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('employeddate') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            {{-- Label:Email , Attributes:email  --}}
                            <div class="col-md-6 form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <label for="email" class="control-label">Email</label>
                                <div>
                                    <input id="email" type="text" class="form-control" name="email"
                                           value="{{ $guard->email }}">
                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-12">
                                <button class="btn btn-default">Edit Guard</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12 col-sm-12 col-xs-12">
            <div class="white-box">
                <h3 class="box-title">Clients</h3>
                <p class="text-muted m-b-30">Assign a client to the guard.</p>
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>Phone Number</th>
                        <th>Email</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($clientswithoutGuards as $client)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $client->name }}</td>
                            <td>{{ $client->phonenumber }}</td>
                            <td>{{ $client->email }}</td>
                            <td>
                                <form action="{{ route('assign.store') }}" method="post">
                                    {{ csrf_field() }}
                                    <input type="hidden" value="{{ $guard->id }}" name="guardid">
                                    <input type="hidden" value="{{ $client->id }}" name="clientid">
                                    <button class="btn btn-info waves-effect waves-light"><span>Assign</span> <i
                                                class="fa fa-forward m-l-5"></i></button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

            </div>
        </div>
    </div>
@endsection