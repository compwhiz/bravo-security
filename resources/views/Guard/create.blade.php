@extends('layouts.bravo')
@section('title')
    Add Guard
@stop
@section('content')
    <div class="container">
        <div class="row">
            <div class="panel panel-default">
                <div class="panel-body">
                    <form action="{{ route('guard.store') }}" method="post">
                        {{ csrf_field() }}
                        {{-- Label:Name , Attributes:name  --}}
                        <div class="col-md-6 form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="control-label">Name</label>
                            <div>
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}">
                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        {{-- Label:Phone Number , Attributes:phonenumber  --}}
                        <div class="col-md-6 form-group{{ $errors->has('phonenumber') ? ' has-error' : '' }}">
                            <label for="phonenumber" class="control-label">Phone Number</label>
                            <div>
                                <input id="phonenumber" type="text" class="form-control" name="phonenumber"
                                       value="{{ old('phonenumber') }}">
                                @if ($errors->has('phonenumber'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('phonenumber') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        {{-- Label:ID number , Attributes:idnumber  --}}
                        <div class="col-md-6 form-group{{ $errors->has('idnumber') ? ' has-error' : '' }}">
                            <label for="idnumber" class="control-label">ID number</label>
                            <div>
                                <input id="idnumber" type="text" class="form-control" name="idnumber"
                                       value="{{ old('idnumber') }}">
                                @if ($errors->has('idnumber'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('idnumber') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        {{-- Label:Employment ID , Attributes:employmentID  --}}
                        <div class="col-md-6 form-group{{ $errors->has('employmentID') ? ' has-error' : '' }}">
                            <label for="employmentID" class="control-label">Employment ID</label>
                            <div>
                                <input id="employmentID" type="text" class="form-control" name="employmentID"
                                       value="{{ old('employmentID') }}">
                                @if ($errors->has('employmentID'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('employmentID') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        {{-- Label:Bank Account , Attributes:bank_account  --}}
                        <div class="col-md-6 form-group{{ $errors->has('bank_account') ? ' has-error' : '' }}">
                            <label for="bank_account" class="control-label">Bank Account</label>
                            <div>
                                <input id="bank_account" type="text" class="form-control" name="bank_account" value="{{ old('bank_account') }}">
                                @if ($errors->has('bank_account'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('bank_account') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        {{-- Label:Employed Date , Attributes:employeddate  --}}
                        <div class="col-md-6 form-group{{ $errors->has('employeddate') ? ' has-error' : '' }}">
                            <label for="employeddate" class="control-label">Employed Date</label>
                            <div>
                                <input id="employeddate" type="date" class="form-control" name="employeddate"
                                       value="{{ old('employeddate') }}">
                                @if ($errors->has('employeddate'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('employeddate') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        {{-- Label:Email , Attributes:email  --}}
                        <div class="col-md-6 form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="control-label">Email</label>
                            <div>
                                <input id="email" type="text" class="form-control" name="email" value="{{ old('email') }}">
                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-12">
                            <button class="btn btn-default">Add Guard</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection