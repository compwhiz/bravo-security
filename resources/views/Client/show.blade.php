@extends('layouts.bravo')
@section('title')
    Client ({{ $clients->name }})
@stop
@section('content')
    <div class="row">
        <div class="col-lg-6 col-sm-6 col-xs-12">
            <div class="white-box">
                <h3 class="box-title">{{ $clients->name }}</h3>
                <p class="text-muted">Phone: <code>{{ $clients->phonenumber }}</code></p>
                <p class="text-muted">Email: <code>{{ $clients->email }}</code></p>
                <p class="text-muted">Start date: <code>{{ $clients->startDate }}</code></p>
                <p class="text-muted">Guards: <code>{{ $clients->guards }}</code></p>
                <p class="text-muted">Daily Rate: <code>{{ $clients->dailyrate }}</code></p>
                <!-- Nav tabs -->
                <ul class="nav customtab nav-tabs" role="tablist">
                    <li role="presentation" class="nav-item">
                        <a href="#home1" class="nav-link active" aria-controls="home" role="tab" data-toggle="tab"
                           aria-expanded="true">
                            <span class="visible-xs"><i class="ti-home"></i></span>
                            <span class="hidden-xs"> Guards</span>
                        </a>
                    </li>
                    <li role="presentation" class="nav-item">
                        <a href="#profile1" class="nav-link"
                           aria-controls="profile" role="tab" data-toggle="tab" aria-expanded="false">
                            <span class="visible-xs"><i
                                        class="ti-user"></i></span> <span class="hidden-xs">History</span></a>
                    </li>
                    <li role="presentation" class="nav-item">
                        <a href="#messages1" class="nav-link" aria-controls="messages" role="tab" data-toggle="tab"
                           aria-expanded="false">
                            <span class="visible-xs"><i class="ti-email"></i></span> <span
                                    class="hidden-xs">Payments</span>
                        </a>
                    </li>
                    <li role="presentation" class="nav-item">
                        <a href="#actions" class="nav-link" aria-controls="home" role="tab" data-toggle="tab"
                           aria-expanded="true">
                            <span class="visible-xs"><i class="ti-home"></i></span>
                            <span class="hidden-xs"> Actions</span>
                        </a>
                    </li>
                </ul>
                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane fade in active show" id="home1" aria-expanded="true">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table class="table table-condensed table-hover">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>Start Date</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach ($clients->soldiers as $client)
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>
                                            <td>
                                                <a href="{{ route('guard.show',$client->mkono->id) }}">{{ $client->mkono->name }}</a>
                                                <span class="label label-table label-danger">active</span>
                                            </td>
                                            <td>{{ $client->startDate->toFormattedDateString() }}</td>
                                            <td>
                                                <form action="" method="post">
                                                    <a href="{{ route('disengageGuard',$client->mkono->id) }}"
                                                       class="btn btn-default">Disengange</a>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>

                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="profile1" aria-expanded="false">
                        <div class="col-md-12">
                            <table class="table table-hover">
                                <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Start Date</th>
                                    <th>End Date</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($history->soldiers as $client)
                                    <tr>
                                        <td>
                                            <a href="{{ route('client.show',$client->mkono->id) }}">{{ $client->mkono->name }}</a>
                                        </td>
                                        <td>{{ $client->startDate->toFormattedDateString() }}</td>
                                        <td>{{ $client->endDate->toFormattedDateString() }}</td>

                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>

                        <div class="clearfix"></div>
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="messages1" aria-expanded="false">

                        <div class="col-md-12">

                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th>Date</th>
                                        <th>Amount</th>
                                        <th>Balance</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($payments as $payment)
                                        @foreach($payment->payments as $item)
                                            <tr>
                                                <td>
                                                    {{$item->startofmonth->toFormattedDateString()}} &mdash; {{$item->endofmonth->toFormattedDateString()}}</td>
                                                <td>{{ number_format($item->amountpaid,2) }}</td>
                                                <td>{{number_format($item->amountpaid,2)}}</td>
                                            </tr>
                                        @endforeach
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="actions" aria-expanded="false">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="">
                                    <?php /** @var \App\Client $clients */ ?>
                                    <form action="{{ route('client.update',$clients->id) }}" method="post">
                                        {{ csrf_field() }}
                                        {{ method_field('PATCH') }}
                                        <div class="col-md-6 form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                            <label for="name" class="control-label">Name</label>
                                            <div class="input-group">
                                                <div class="input-group-addon"><i class="ti-user"></i></div>
                                                <input id="name" type="text" class="form-control" name="name"
                                                       value="{{ $clients->name }}">
                                            </div>
                                            @if ($errors->has('name'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('name') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                        {{-- Label:Email , Attributes:email  --}}
                                        <div class="col-md-6 form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                            <label for="email" class="control-label">Email</label>
                                            <div class="input-group">
                                                <div class="input-group-addon"><i class="ti-email"></i></div>
                                                <input id="email" type="text" class="form-control" name="email"
                                                       value="{{ $clients->email }}">
                                            </div>
                                            @if ($errors->has('email'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('email') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                        {{-- Label:Phone Number , Attributes:phone_number  --}}
                                        <div class="col-md-6 form-group{{ $errors->has('phone_number') ? ' has-error' : '' }}">
                                            <label for="phone_number" class="control-label">Phone Number</label>
                                            <div class="input-group">
                                                <div class="input-group-addon"><i class="ti-mobile"></i></div>
                                                <input id="phone_number" type="text" class="form-control"
                                                       name="phone_number"
                                                       value="{{ $clients->phonenumber }}">
                                            </div>
                                            @if ($errors->has('phone_number'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('phone_number') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                        {{-- Label:Guards , Attributes:guards  --}}
                                        <div class="col-md-6 form-group{{ $errors->has('guards') ? ' has-error' : '' }}">
                                            <label for="guards" class="control-label">Guards</label>
                                            <div class="input-group">
                                                <div class="input-group-addon"><i class="ti-server"></i></div>
                                                <input id="guards" type="text" class="form-control" name="guards"
                                                       value="{{ $clients->guards }}">
                                            </div>
                                            @if ($errors->has('guards'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('guards') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                        {{-- Label:Start Date , Attributes:start_date  --}}
                                        <div class="col-md-6 form-group{{ $errors->has('start_date') ? ' has-error' : '' }}">
                                            <label for="start_date" class="control-label">Start Date</label>
                                            <div class="input-group">
                                                <div class="input-group-addon"><i class="ti-alarm-clock"></i></div>
                                                <input id="start_date" type="text" class="form-control"
                                                       name="start_date"
                                                       value="{{ $clients->startDate }}">
                                            </div>
                                            @if ($errors->has('start_date'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('start_date') }}</strong>
                                                </span>
                                            @endif
                                        </div>

                                        {{-- Label:End Date , Attributes:end_date  --}}
                                        <div class="col-md-6 form-group{{ $errors->has('end_date') ? ' has-error' : '' }}">
                                            <label for="end_date" class="control-label">End Date</label>
                                            <div class="input-group">
                                                <div class="input-group-addon"><i class="ti-alarm-clock"></i></div>
                                                <input id="end_date" type="text" class="form-control" name="end_date"
                                                       value="{{ $clients->endDate }}">
                                            </div>
                                            @if ($errors->has('end_date'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('end_date') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                        {{-- Label:Rate , Attributes:rate  --}}
                                        <div class="col-md-6 form-group{{ $errors->has('rate') ? ' has-error' : '' }}">
                                            <label for="rate" class="control-label">Rate</label>
                                            <div class="input-group">
                                                <div class="input-group-addon"><i class="ti-alarm-clock"></i></div>
                                                <input id="rate" type="text" class="form-control" name="rate"
                                                       value="{{ $clients->dailyrate }}">
                                            </div>
                                            @if ($errors->has('rate'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('rate') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                        <input type="hidden" name="id" value="{{ $clients->id }}">
                                        <div class="col-md-12">
                                            <button type="submit"
                                                    class="btn btn-success waves-effect waves-light m-r-10">
                                                Edit Client
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12" style="margin-top: 2em;">
                            <div class="">
                                <form action="{{ route('client.destroy',$clients->id) }}" method="post">
                                    {{ csrf_field() }}
                                    {{ method_field('DELETE') }}
                                    {{-- Label:Date , Attributes:date  --}}
                                    <div class="col-md-6 form-group{{ $errors->has('date') ? ' has-error' : '' }}">
                                        <label for="date" class="control-label">Date</label>
                                        <div>
                                            <input id="date" type="date" class="form-control" name="date"
                                                   value="{{ old('date') }}">
                                            @if ($errors->has('date'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('date') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <button class="btn btn-danger">Terminate</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-sm-6 col-xs-12">
            <div class="white-box">
                <h3 class="box-title">Guards</h3>
                <p class="text-muted m-b-30">Assign guards to the client.</p>
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="table-responsive">
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            {{--<th>Phone Number</th>--}}
                            {{--<th>Email</th>--}}
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>

                        @foreach (\App\Guards::all()->where('client_id',NULL) as $guard)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $guard->name }}</td>
{{--                                <td>{{ $guard->phonenumber }}</td>--}}
{{--                                <td>{{ $guard->email }}</td>--}}
                                <td>
                                    <form action="{{ route('assign.store') }}" method="post">
                                        {{ csrf_field() }}
                                        <input type="hidden" value="{{ $guard->id }}" name="guardid">
                                        <input type="hidden" value="{{ $clients->id }}" name="clientid">
                                        <button class="btn btn-info waves-effect waves-light"><span>Assign</span> <i
                                                    class="fa fa-forward m-l-5"></i></button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection