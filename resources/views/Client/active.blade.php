@extends('layouts.bravo')
@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="white-box">
                <h3 class="box-title m-b-0">Clients</h3>
                <p class="text-muted m-b-30">All Active Clients</p>
                <div class="table-responsive">
                    <table class="table table-condensed table-hover">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Phone Number</th>
                            <th>Email</th>
                            <th>Guards</th>
                            <th>Start Date</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php /** @var \App\Client $client */ ?>
                        @foreach ($clients as $client)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td><a href="{{ route('client.show',$client->id) }}">{{ $client->name }}</a></td>
                                <td>{{ $client->phonenumber }}</td>
                                <td>{{ $client->email }}</td>
                                <td>{{ $client->guards }}</td>
                                <td>{{ $client->startDate }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <p>Showing {{ $clients->count() }} Clients of {{ $clients->total() }}</p>
                    <div class="pull-right">
                        {{ $clients->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
