@extends('layouts.bravo')
@section('title')
    Dashboard
@stop

@section('content')
    <div class="row">
        <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
            <div class="white-box">
                <h3 class="box-title">Clients</h3>
                <div class="text-right"><span class="text-muted">Active Clients</span>
                    <h1><sup><i class="ti-arrow-up text-success"></i></sup> {{ \App\Client::all()->count() }}</h1>
                </div>
                <span class="text-success">20%</span>
                <div class="progress m-b-0">
                    <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="50"
                         aria-valuemin="0" aria-valuemax="100" style="width:20%;"><span
                                class="sr-only">20% Complete</span></div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
            <div class="white-box">
                <h3 class="box-title">Guards</h3>
                <div class="text-right"><span class="text-muted">Active Guards</span>
                    <h1><sup><i class="ti-arrow-down text-danger"></i></sup> {{ \App\Guards::all()->count() }}</h1>
                </div>
                <span class="text-danger">30%</span>
                <div class="progress m-b-0">
                    <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="50"
                         aria-valuemin="0" aria-valuemax="100" style="width:30%;"><span
                                class="sr-only">230% Complete</span></div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <div class="white-box">
                <h3 class="box-title">Money</h3>
                <div class="text-right">
                    <span class="text-muted"> Received Money Last Month</span><br>
                    <span class="text-muted"> Aug, 2018</span>
                    <h1>
                        <sup><i class="ti-credit-card text-success"></i></sup>
                        {{ number_format($whereMonth->sum('amountpaid'),2) }}
                    </h1>
                    <hr>
                </div>
            </div>
        </div>

    </div>
@stop