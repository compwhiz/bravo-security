@extends('layouts.bravo')
@section('content')
    <div class="row">
        <div class="panel panel-default">
            <div class="panel-body">
                <form action="" method="post">
                    {{ csrf_field() }}
                    {{-- Label:Name , Attributes:name  --}}
                    <div class="col-md-6 form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                        <label for="name" class="control-label">Name</label>
                        <div>
                            <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}">
                            @if ($errors->has('name'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    {{-- Label:Age , Attributes:age  --}}
                    <div class="col-md-6 form-group{{ $errors->has('age') ? ' has-error' : '' }}">
                        <label for="age" class="control-label">Age</label>
                        <div>
                            <input id="age" type="text" class="form-control" name="age" value="{{ old('age') }}">
                            @if ($errors->has('age'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('age') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    .col-md-12
                </form>
            </div>
        </div>
    </div>
@endsection