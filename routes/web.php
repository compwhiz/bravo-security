<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {

    return redirect('login');

//    return view('frontend.home');

});

Route::get('newassign', function () {

    $client = \App\Client::orderBy('name')->with('assignment.mkono')->get();

    $guards = \App\Guards::has('client')->get();
    return view('pdf.equipment', compact('client','guards'));

});

Auth::routes();


Route::group(['middleware'=> 'auth'], function () {

    Route::group(['prefix' => 'admin'], function () {

        Route::get('/', function () {

            $whereMonth = \App\Payments::whereMonth('startofmonth', '08')->get();
            return view('welcome',compact('whereMonth'));
        });

        Route::resource('reports','ReportController');

        Route::group(['prefix'=>'print'], function () {

            Route::get('payments', function () {
                $clientE = \App\Client::with('payments.payment')->get();
                return view('pdf.payments', compact('clientE'));

            });
        });

        //Client Routes

        Route::get('client/payments', 'ClientController@payments')->name('payments');

        Route::get('client/active', 'ClientController@activeClient')->name('activeclient');

        Route::get('client/inactive', 'ClientController@inactiveClient')->name('inactiveclient');

        Route::resource('client', 'ClientController');

        Route::resource('payments', 'PaymentsController');

        Route::resource('paids', 'PaidController');

        //Guard Routes

        Route::resource('guard/transfer', 'GuardtransferController');

        Route::get('guard/management/disengange/{guards}', 'GuardsController@disengage')->name('disengageGuard');

        Route::get('guard/management/assign', 'GuardsController@assigns')->name('assignGuard');

        Route::get('guard/management/unassigned', 'GuardsController@unassignedguard')->name('unassignedGuard');

        Route::resource('guard', 'GuardsController');

        Route::resource('attendance', 'AttendanceController');


        //Assignment routes

        Route::resource('assign', 'AssignmentController');

        Route::get('/home', 'HomeController@index')->name('home');
    });
});


