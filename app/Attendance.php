<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Attendance extends Model
{
    public function soldier()
    {
        return $this->hasOne(Guards::class,'id','guard_id');
    }
}
