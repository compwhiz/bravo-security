<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use function Symfony\Component\Debug\Tests\testHeader;

/**
 * App\Assignment
 *
 * @property int $id
 * @property int $client_id
 * @property int $guard_id
 * @property string $startDate
 * @property string|null $endDate
 * @property string $status
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Assignment whereClientId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Assignment whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Assignment whereEndDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Assignment whereGuardId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Assignment whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Assignment whereStartDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Assignment whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Assignment whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \App\Guards $mkono
 */
class Assignment extends Model
{

    protected $dates = [
        'endDate',
        'startDate'
    ];
    protected $guarded = [];
    protected $appends = ['days_worked'];

    public function mkono()
    {
        return $this->hasOne(Guards::class, 'id', 'guard_id')
            ->orderBy('name')->withDefault(function () {
                return null;
            });
    }

    public function client()
    {
        return $this->hasOne(Client::class, 'id', 'client_id');
    }

    public function trashedClient()
    {
        return $this->hasOne(Client::class, 'id', 'client_id')->withTrashed()->withDefault(function () {
            return null;
        });
    }

}
