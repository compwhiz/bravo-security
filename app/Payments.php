<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Payments
 *
 * @property int $id
 * @property int $client_id
 * @property int $amountpaid
 * @property string $startofmonth
 * @property string $endofmonth
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\Client $clients
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Payments whereAmountpaid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Payments whereClientId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Payments whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Payments whereEndofmonth($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Payments whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Payments whereStartofmonth($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Payments whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Payments extends Model
{
    protected $dates = [
        'startofmonth','endofmonth'
    ];

    public function clients()
    {
        return $this->hasOne(Client::class, 'id', 'client_id');
    }

    public function payment()
    {
        return $this->hasMany(Paid::class, 'payments_id', 'id');
    }
}
