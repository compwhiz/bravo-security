<?php

namespace App\Http\Controllers;

use App\Client;
use App\Events\ClientTerminated;
use App\Payments;
use Illuminate\Http\Request;

class ClientController extends Controller
{
    protected $client;
    protected $payments;

    public function __construct(Client $client, Payments $payments)
    {

        $this->client = $client;
        $this->payments = $payments;
    }

    public function payments()
    {
        $all = $this->payments::with(['clients','payment'])->get();
        return view('Client.payments', compact('all'));
    }

    public function activeClient()
    {
        $clients = $this->client::whereHas('soldiers', function ($query) {
            $query->whereNull('endDate');
        })->paginate(100);
        return view('Client.active', compact('clients'));
    }

    public function inactiveClient()
    {
        $clients = $this->client->onlyTrashed()->paginate(10);
        return view('Client.inactive', compact('clients'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $clients = $this->client::paginate(100);
        return view('Client.index', compact('clients'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Client.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Client $client)
    {
        $patterns = [
            '/^(?:254)(\d{9})/is',
        ];

        $this->validate($request, [
            'name' => ['required', 'unique:clients,name'],
            'start_date' => ['required', 'date'],
            'email' => ['required', 'email'],
            'phone_number' => ['required', function ($attribute, $value, $fail) use ($patterns) {
                foreach ($patterns as $pattern) {
                    if (preg_match($pattern, $value, $matches, PREG_OFFSET_CAPTURE, 0)) {
                        return;
                    }
                    $fail(':attribute must start with 254');
                }
            }, 'unique:clients,phonenumber'],
            'end_date' => ['required', 'date', 'After:start_date', 'nullable'],
            'guards' => ['required', 'numeric'],
            'rate' => ['required'],
        ]);

        $client->name = $request->name;
        $client->startDate = $request->start_date;
        $client->endDate = $request->end_date;
        $client->guards = $request->guards;
        $client->phonenumber = $request->phone_number;
        $client->email = $request->email;
        $client->dailyrate = $request->rate;
        $client->saveOrFail();

        $client->notify();

        return redirect()->route('client.show', $client);

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Client $client
     * @return \Illuminate\Http\Response
     */
    public function show(Client $client)
    {
        $clients = $this->ActiveClients($client);

        $history = $this->ClientHistory($client);

        $payments = $client::with(['payments'])->where('id',$client->id)->get();

        return view('Client.show', compact('clients', 'history','payments'));
    }

    /**
     * @param Client $client
     * @return \Illuminate\Database\Eloquent\Model|null|static
     */
    protected function ActiveClients(Client $client)
    {
        return $client::with(['soldiers' => function ($query) {
            $query->whereNull('endDate');
        }])->where('id', $client->id)->first();
    }

    /**
     * @param Client $client
     * @return \Illuminate\Database\Eloquent\Model|null|static
     */
    protected function ClientHistory(Client $client)
    {
        return $client::with(['soldiers' => function ($query) {
            $query->whereNotNull('endDate');
        }])->where('id', $client->id)->first();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Client $client
     * @return \Illuminate\Http\Response
     */
    public function edit(Client $client)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Client $client
     * @return \Illuminate\Http\Response
     */
    public function destroy(Client $client, Request $request)
    {

        // Send an event that the client has terminated the service.
        // This will in turn disengange all the guards attached to the client.
        ClientTerminated::dispatch($client, $request);
        $client->delete();

        return redirect()->route('client.index');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Client $client
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Client $client)
    {
        $client->name = $request->name;
        $client->startDate = $request->start_date;
        $client->endDate = $request->end_date;
        $client->guards = $request->guards;
        $client->phonenumber = $request->phone_number;
        $client->email = $request->email;
        $client->dailyrate = $request->rate;
        $client->saveOrFail();

        return back();
    }
}
