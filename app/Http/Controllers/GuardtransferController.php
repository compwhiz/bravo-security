<?php

namespace App\Http\Controllers;

use App\Assignment;
use App\Guards;
use App\Guardtransfer;
use Illuminate\Http\Request;

class GuardtransferController extends Controller
{
    protected $guards;

    function __construct(Guards $guards)
    {
        $this->guards = $guards;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('transfers.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Assignment $assignment, Guards $guards)
    {

        $this->validate($request,[
            'date' => ['required','date']
        ]);

        $guard = $guards->find($request->firstguard);

        $this->guardOneTransfer($request, $guard);

        $guardTwo = $guards->find($request->guard);

        $this->guardTwoTransfer($request, $guardTwo);

        return response()->json('Guard transferred');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Guardtransfer $guardtransfer
     * @return \Illuminate\Http\Response
     */
    public function show(Guardtransfer $guardtransfer)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Guardtransfer $guardtransfer
     * @return \Illuminate\Http\Response
     */
    public function edit(Guardtransfer $guardtransfer)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Guardtransfer $guardtransfer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Guardtransfer $guardtransfer)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Guardtransfer $guardtransfer
     * @return \Illuminate\Http\Response
     */
    public function destroy(Guardtransfer $guardtransfer)
    {
        //
    }

    /**
     * @param Request $request
     * @param $guard
     */
    protected function guardOneTransfer(Request $request, $guard): void
    {
        $guard->currentassignments()->update([
            'endDate' => $request->date
        ]);

        $guard->assignments()->create([
            'client_id' => $request->to,
            'startDate' => $request->date,
        ]);

        $guard->update([
            'client_id' => $request->to
        ]);
    }

    /**
     * @param Request $request
     * @param $guardTwo
     */
    protected function guardTwoTransfer(Request $request, $guardTwo): void
    {
        $guardTwo->currentassignments()->update([
            'endDate' => $request->date
        ]);

        $guardTwo->update([
            'client_id' => null
        ]);
    }
}
