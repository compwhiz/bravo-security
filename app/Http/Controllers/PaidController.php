<?php

namespace App\Http\Controllers;

use App\Paid;
use Illuminate\Http\Request;

class PaidController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws \Throwable
     */
    public function store(Request $request, Paid $paid)
    {
        //datepaid
        //invoice
        //amount
        //cheque

        $paid->payments_id = $request->id;
        $paid->date = $request->datepaid;
        $paid->amount = $request->amount;
        $paid->extras = 'NA';
        $paid->chequeno = $request->cheque;
        $paid->receivedBy = 'Dennis Mwanza Mutua';
        $paid->saveOrFail();

        return response()->json($paid);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Paid  $paid
     * @return \Illuminate\Http\Response
     */
    public function show(Paid $paid)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Paid  $paid
     * @return \Illuminate\Http\Response
     */
    public function edit(Paid $paid)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Paid  $paid
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Paid $paid)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Paid  $paid
     * @return \Illuminate\Http\Response
     */
    public function destroy(Paid $paid)
    {
        //
    }
}
